const colorsClassArray = ['red', 'red', 'blue', 'blue', 'greenyellow', 'greenyellow', 'brown', 'brown', 'orange', 'orange', 'pink', 'pink', 'purple', 'purple', 'magenta', 'magenta']

// Shuffling Array 
const shuffleArray = (array) => {

    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1))
        const temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array
}

// Input the number of cards from user using enter button
cardNumber = document.getElementById('cardNumber');

cardNumber.addEventListener('keypress', function (event) {

    if (event.charCode == 13) {

        requiredNumberOfCards = parseInt(cardNumber.value)

        if (requiredNumberOfCards % 2 != 0) {
            requiredNumberOfCards += 1
        }

        if (requiredNumberOfCards < 3 || requiredNumberOfCards > 16 || requiredNumberOfCards == '') {
            cardNumber.placeholder = 'Invalid'
            cardNumber.nextElementSibling.nextElementSibling.style.display = "block"
        }
        else {
            cardNumber.nextElementSibling.nextElementSibling.style.display = "none"

            cardNumber.placeholder = ''
            cardNumber.value = ''
            cardNumber.disabled = true;

            // Accessing Grid Container
            let gridContainer = document.querySelector('.game-grid-container');

            // Adding all div elements in grid container 
            presentColorArray = colorsClassArray.slice(0, requiredNumberOfCards)
            shuffleArray(presentColorArray)

            for (i = 0; i < requiredNumberOfCards; i++) {

                let newDiv = document.createElement('div');
                newDiv.classList.add("initial-color");
                newDiv.classList.add("card-container");


                let newCard = document.createElement('div');
                newCard.classList.add("card", presentColorArray[i]);
                newCard.setAttribute('color', `${presentColorArray[i]}`)
                newCard.setAttribute("onclick", `openCard(this)`);

                newDiv.appendChild(newCard)
                gridContainer.appendChild(newDiv);
            }

            // Changing color of div on click
            cards = document.getElementsByClassName('card')

            for (i = 0; i < cards.length; i++) {

                cards[i].addEventListener('click', function (event) {

                    event.target.parentElement.classList.add("change-color");
                })
            }
            cardNumber.nextElementSibling.disabled = true

            startTimer()
        }
    }
})

// Input the number of cards from user using enter button
submitButton = document.getElementById('submit-button');

submitButton.addEventListener('click', function (event) {

    cardNumber = submitButton.previousElementSibling

    requiredNumberOfCards = parseInt(cardNumber.value)

    if (requiredNumberOfCards % 2 != 0) {
        requiredNumberOfCards += 1
    }

    if (requiredNumberOfCards < 3 || requiredNumberOfCards > 16 || requiredNumberOfCards == '') {
        cardNumber.placeholder = 'Invalid';
        submitButton.nextElementSibling.style.display = "block"
    }
    else {
        submitButton.nextElementSibling.style.display = "none"

        cardNumber.placeholder = ''
        cardNumber.value = ''
        cardNumber.disabled = true;

        // Accessing Grid Container
        let gridContainer = document.querySelector('.game-grid-container');

        // Adding all div elements in grid container 
        presentColorArray = colorsClassArray.slice(0, requiredNumberOfCards)
        shuffleArray(presentColorArray)

        for (i = 0; i < requiredNumberOfCards; i++) {

            let newDiv = document.createElement('div');
            newDiv.classList.add("initial-color");
            newDiv.classList.add("card-container");


            let newCard = document.createElement('div');
            newCard.classList.add("card", presentColorArray[i]);
            newCard.setAttribute('color', `${presentColorArray[i]}`)
            newCard.setAttribute("onclick", `openCard(this)`);

            newDiv.appendChild(newCard)
            gridContainer.appendChild(newDiv);
        }

        // Changing color of div on click
        cards = document.getElementsByClassName('card')

        for (i = 0; i < cards.length; i++) {

            cards[i].addEventListener('click', function (event) {

                event.target.parentElement.classList.add("change-color");
            })
        }
        submitButton.disabled = true
        startTimer()
    }
})
let moves = document.getElementById('current-moves')
let currentMoves = 0

// Main Logic
memory = []
count = 0
function openCard(card) {
    currentMoves += 1
    moves.innerText = currentMoves



    // card.parentElement.classList.add("change-color");

    // Removing click event from element
    card.setAttribute("onclick", `false`);

    memory.push(card)

    if (memory.length == 2) {

        if (memory[0].getAttribute('color') === memory[1].getAttribute('color')) {

            // Disabling hover
            memory[1].parentElement.classList.remove('card-container');
            memory[0].parentElement.classList.remove('card-container');

            // Printing colors
            console.log(memory[0].getAttribute('color'))
            console.log(memory[1].getAttribute('color'))

            // Icreasing counter and clearing memory
            count += 2

            memory = []
        }
        else {

            setTimeout(() => {

                memory[0].parentElement.classList.remove('change-color');
                memory[1].parentElement.classList.remove('change-color');

                // Resetting onclick properties for cards
                memory[0].setAttribute("onclick", `openCard(this)`);
                memory[1].setAttribute("onclick", `openCard(this)`);

                memory = []

            }, 400)
        }
    }

    let numberOfCards = document.getElementsByClassName('card').length

    if (count == numberOfCards) {

        gridContainer.style.display = 'none'
        inputForm.style.display = 'flex';
        cardNumber.style.display = 'none';
        submitButton.style.display = 'none';
        inputHeader.innerHTML = "Congratulations !! <br> You Won !!"

        setTimeout(() => {            

            location.reload()
        }, 2000)
    }
}

let currentSeconds = 0;
let currentMinutes = 0;

let inputHeader = document.getElementById('input-header');
let resetButton = document.getElementById('reset-button');
let seconds = document.getElementById('seconds');
let minutes = document.getElementById('minutes');
let inputForm = document.getElementById('input-form');
let gridContainer = document.getElementById('grid-container');
let movesContainer = document.getElementById('moves');
let timeContainer = document.getElementById('time');
function startTimer(){

    resetButton.style.display = 'block';
    inputForm.style.display = 'none';
    gridContainer.style.display = 'flex';
    movesContainer.style.display = "block"
    timeContainer.style.display = "block"

    setInterval(function(){

        currentSeconds +=1

        if(currentSeconds == 60){

            currentSeconds = 0
            currentMinutes +=1

            minutes.innerText = currentMinutes
        }

        seconds.innerText = currentSeconds


    }, 1000);
}

function reset(){
    location.reload()
}